import java.util.Scanner;

public class ShutTheBox{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to Shut the Box");
		
		Board b = new Board();
		boolean gameOver = false;
		
		do{
			// Player 1 turn
			System.out.println("Player One's Turn");
			System.out.println(b.toString());
			//b.playATurn();
			
			if (b.playATurn() == true){
				gameOver = true;
				System.out.println("Player Two Wins!");
				break;
			}
			else{
				System.out.println();
			}
			
			// Player 2 turn
			System.out.println("Player Two's Turn");
			System.out.println(b.toString());
			//b.playATurn();
			
			if (b.playATurn() == true){
				gameOver = true;
				System.out.println("Player One Wins!");
				break;
			}
			else{
				System.out.println();
			}			
		}while(gameOver == false);
	}
}