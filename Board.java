public class Board{
	private Die dieOne;
	private Die dieTwo;
	private boolean[] closedTiles; // represents tiles #ed 1-12. will store values rolled in game
	
	public Board(){
		this.dieOne = new Die();
		this.dieTwo = new Die();
		this.closedTiles = new boolean[12];
	}
	
	public String toString(){
		String test = "";
		for(int i=0; i<closedTiles.length; i++){
			if (!closedTiles[i]){
				test+="[" +(i+1)+ "] ";
			}
			else{
				test+="[X] ";
			}
		}
		return test;
	}
	
	public boolean playATurn(){
		boolean playATurn = false;
		dieOne.roll();
		System.out.println(dieOne.toString());
		dieTwo.roll();
		System.out.println(dieTwo.toString());
		int d1 = dieOne.getPips();
		int d2 = dieTwo.getPips();
		System.out.println("Dice total: " + (d1+d2));
		
		if (closedTiles[(d1+d2)-1] == false){
			System.out.println("Closing tile: " +(d1+d2));
			closedTiles [(d1+d2)-1]= true;
		}
		else{
			System.out.println("Tile already closed!");
			playATurn = true;
		}
		
		return playATurn;
	}
}