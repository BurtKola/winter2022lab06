import java.util.Random;

public class Die{
	private int pips; //number of dots on die face
	private Random rand;
	
	public Die(){
		this.pips = 1;
		this.rand = new Random();
	}
	
	public int getPips(){
		return pips;
	}
	
	public void roll(){
		pips = rand.nextInt(6);
		pips++;
	}
	
	public String toString(){
		return pips+"";
	}
}